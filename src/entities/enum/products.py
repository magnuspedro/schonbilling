from enum import Enum

class Products(Enum):
    SHAMPOO = 'shampoo'
    CONDITIONER = 'conditioner'
    FINISHER = 'finisher'
    TREATMENT = 'treatment'
