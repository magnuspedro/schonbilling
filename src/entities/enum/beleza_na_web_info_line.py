from enum import Enum


class InfoLine(Enum):
    CATEGORY = 'Categorias'
    HAIR_TYPE = 'Tipos de Cabelo'
    HAIR_SHAFT_CONDITION = 'Condição dos Fios'
    UTILITY = 'Desejo de Beleza'
    PROPRIETIES = 'Propriedades'
    SIZE = 'Tamanho'
    BRAND = 'Marca'
    LINE = 'Linha'
    TEXTURE = 'Textura'
